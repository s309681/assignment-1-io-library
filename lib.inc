section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov r11, 0      ;очищаем
    xor rax, rax
    .loop:
    cmp byte [rdi+rax], 0 
    mov r11b, byte [rdi+rax]
    je .end 
    inc rax 
    jmp .loop
    .end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    xor rax, rax
    ret



; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rax, 1
    mov rdi, 1
    mov rdx, 1
    syscall 
    pop rdi
    xor rax, rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
print_uint:
    mov r8, rsp
    push 0
    mov rax, rdi
    mov rcx, 10
    .print_uint_loop:
    mov rdx, 0
    mov rcx, 10
    div rcx
    add rdx, 48
    dec rsp
    mov [rsp], dl    ;младший байт rdx
    test rax, rax
    jnz .print_uint_loop
    mov rdi, rsp
    call print_string
    mov rsp, r8
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, -1
    js .neg_num
    call print_uint
    ret
    .neg_num:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor r8, r8
    xor r9, r9
    .string_equals_loop:
    mov r8b, byte [rdi]
    mov r9b, byte [rsi]
    cmp r8, r9
    jnz .string_equals_end_0
    inc rdi
    inc rsi
    cmp r8, 0
    jnz .string_equals_loop
    mov rax, 1
    ret
    .string_equals_end_0:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    push rdi
    push rsi

    .space_character_loop:
    call read_char
    cmp rax, 0x9
    jz .space_character_loop
    cmp rax, 0xa
    jz .space_character_loop
    cmp rax, 0x20
    jz .space_character_loop

    pop rsi
    pop rdi
    xor r8, r8
    .read_word_loop:
    cmp rax, 0
    jz .read_word_end_size
    cmp rax, 0x9
    jz .read_word_end_size
    cmp rax, 0xa
    jz .read_word_end_size
    cmp rax, 0x20
    jz .read_word_end_size
    cmp rsi, r8
    jz .read_word_end_0
    mov [rdi], rax
    inc r8
    inc rdi
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    jmp .read_word_loop

    .read_word_end_size:
    pop rax
    mov rdx, r8
    ret

    .read_word_end_0:
    pop rax
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    push rdi
    mov r8, rdi
    mov r10, dword 0
    push 0
    .parse_uint_loop:
    xor rax, rax
    mov al, byte [r8 + r10] ;last byte rax (al)
    sub rax, 48
    cmp rax, 0
    jb .parse_uint_end
    cmp rax, byte 9
    ja .parse_uint_end 
    mov rdi, rax
    pop rax
    mov r11, rax
    sal rax, 3
    add rax, r11
    add rax, r11
    add rdi, rax
    push rdi
    inc r10
    jmp .parse_uint_loop

    .parse_uint_end:
    pop rax
    pop rdi
    mov rdx, r10
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
    cmp al, byte 45
    jz .parse_int_neg_word
    jmp parse_uint

    .parse_int_neg_word:
    inc rdi
    call parse_uint
    neg rax
    cmp rdx, 0
    jnz .parse_int_end_not_0
    xor rdx, rdx
    ret
    .parse_int_end_not_0:
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    inc rax
    mov r8, rax
    mov r9, rax
    pop rdx
    cmp rdx, rax
    jb .nomatch_size_end

    .string_copy_loop:
    pop rsi
    pop rdi
    mov r10b, byte [rdi]
    mov [rsi], r10b
    inc rdi
    inc rsi
    push rdi
    push rsi
    dec r8
    cmp r8, 0
    jnz .string_copy_loop
    pop rdi
    pop rsi
    mov rax, r9
    ret

    .nomatch_size_end:
    pop rdi
    pop rsi
    xor rax, rax
    ret
